require 'cinch'
require 'dotenv'
require './hello_answer'

class Hello
  include Cinch::Plugin
  set :prefix, //
  match(/(hello)|(bonjour)|(coucou)/i)

  def execute(m)
    m.reply answer(m.user.nick)
  end

  def answer(nick)
    HelloAnswer.new(nick).to_s
  end
end
