require 'cinch'
require 'dotenv'
require './hello_plugin'
Dotenv.load

bot = Cinch::Bot.new do
  configure do |c|
    c.server = ENV['IRC_SERVER']
    c.channels = [ENV['CHANNEL']]
    c.sasl.username = ENV['SASL_USERNAME']
    c.sasl.password = ENV['SASL_PASSWORD']
    c.nick = ENV['NICK']
    c.plugins.plugins = [Hello]
  end
end

bot.start
