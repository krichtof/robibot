require './hello_answer'

describe HelloAnswer do
  let(:subject) { described_class.new('John') }
  let(:surnames) { ['Jo', 'Johnny', 'Jojo'] }

  it "should reply with a sample surname" do
    answer_regex = /((salut)|(bonjour)|(hello))+ jo/i
    expect(subject.to_s).to(satisfy{ |a| answer_regex =~ a })
  end
end

