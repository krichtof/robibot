require 'dotenv'
Dotenv.load

class HelloAnswer
  def initialize(name)
    @name = name
  end

  def to_s
    answer
  end

  private
  def answer
    "#{greeting_word} #{surname} !".capitalize
  end

  def greeting_word
    %w(salut bonjour hello).sample
  end

  def surname
    surnames.sample || @name
  end

  def surnames
    if ENV[@name.upcase]
      ENV[@name.upcase].split(',').map(&:strip)
    else
      [ @name ]
    end
  end
end

